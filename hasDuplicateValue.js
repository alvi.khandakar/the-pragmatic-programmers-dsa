// An exponential funciton that is extremely wasteful

function hasDuplicateValueExp(array) { 
  let steps = 0; // count of steps
  for(let i = 0; i < array.length; i++) {
    for(let j = 0; j < array.length; j++) {
      steps++; // increments number of steps
      if(i !== j && array[i] === array[j]) {
        return true;
      }
    }
  }
  console.log("Number of steps: ", steps); // print steps if no dupes
  return false;
}

hasDuplicateValueExp([1,3,5,6,7,8,9,10]) // Number of steps = 64

function hasDuplicateValueLinear(array) {
  let steps = 0;
  let existingNumbers = [];

  for(let i = 0; i < array.length; i++) {
    steps++;
    if(existingNumbers[array[i]] == 1) {
      return true;
    } else {
      existingNumbers[array[i]] = 1;
    }
  }
  console.log(steps); // print steps if no duplicate
  return false;
}

hasDuplicateValueLinear([1,3,5,6,7,8,9,10]) // number of steps = 8
def binary_search(array, search_value)
  # First, establish lower and upper bounds of where the value can exist
  # To start, lower bound == first index, upper bound == last index

  lower_bound = 0
  upper_bound = array.length - 1

  while lower-bound <= upperbound do

    # Find midpoint between upper and lower bounds
    # (( Don't have to worry about result being a non-integer in Ruby because it rounds to nearest integer))

    midpoint = (upper_bound + lower_bound) / 2

    # 
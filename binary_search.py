# Translated from Ruby
def binary_search(array, target_value):
    
  # first establish lower and upper bounds
  # this is the realm where the search will occur
  # Lower == index 0, upper == final index

  lower_bound = 0
  upper_bound = len(array) - 1

  # Begin loop in which we inspect the middlemost value
  # this would be the midpoint b/w upper and lower bounds

  while lower_bound <= upper_bound:

    # Find the midpoint b/w upper and lower bounds
    # In Python, unlike Ruby, you have to manually 
    # round to the nearest integer, using //

    midpoint = (upper_bound + lower_bound) // 2
    print('this is midpoint', midpoint)

    # now inspect the value at midpoint

    value_midpoint = array[midpoint]

    # if midpoint val == target_value -> Done!
    # else: change lower or upper based on whether we need to
    # go higher or lower, that happens next

    if target_value == value_midpoint:
      return midpoint
    elif target_value < value_midpoint:
      upper_bound = midpoint - 1
    elif target_value > value_midpoint:
      lower_bound = midpoint + 1

  return None

print(binary_search([3, 17, 75, 80, 202], 17))